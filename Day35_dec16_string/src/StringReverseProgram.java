
public class StringReverseProgram {
	public static void main(String[] args) {
		String message = "Hello";
		StringBuilder sb = new StringBuilder();
		for (int i = message.length() - 1; i >= 0; i--) {
			sb.append(message.charAt(i));
		}

		String rev = sb.toString();
		System.out.println(rev);
	}
}
