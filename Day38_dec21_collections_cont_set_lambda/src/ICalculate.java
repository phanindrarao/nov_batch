
public interface ICalculate {
	int add(int a, int b);

	public static void main(String[] args) {
		// example to 
		ICalculate i = (a, b) -> a + b;

		int addition = i.add(100, 250);
		System.out.println(addition); // 350
	}
}
