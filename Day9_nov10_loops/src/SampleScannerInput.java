import java.util.Scanner;

public class SampleScannerInput {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter a number");
		int x = s.nextInt();
		System.out.println(x);

		// it will accept one word input
		System.out.println("Enter a word");
		String message = s.next();
		System.out.println("The message is " + message);
	}

}
