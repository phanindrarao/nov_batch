
public class TernaryExample3 {
	public static void main(String[] args) {
		int age = 101;
		System.out.println((age >= 0 && age <= 12) ? "child" : (age >= 13 && age <= 100) ? "adult" : "wrong value");
	}
}
