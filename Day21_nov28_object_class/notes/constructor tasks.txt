1. One constructor can call only one another constructor either (this()) 
or super()

2. constructor call must be the first executable line of code 

3. In a class, a minimum of one constructor must not call the same class 
constructor (using this()) you will get 
RECURSIVE CONSTRUCTOR INVOCATION
COMPILER ERROR.