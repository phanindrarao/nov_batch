
public class Watch {
	String color = "black";
	String brand = "Fossil";
	double price = 24995;

	void switchOn() {
		System.out.println("Switching on the watch");
	}

	void switchOff() {
		System.out.println("Switching off the watch");
	}

	public static void main(String[] args) {
		Watch w1 = new Watch();
		Watch w2 = new Watch();
		Watch w3 = new Watch();
		System.out.println(w1.color); // black
		System.out.println(w3.price); // 24995.0
		w3.price = 20000;
		System.out.println(w3.price); // 20000.0
		w2.switchOn(); // Switching on the watch
		w2.switchOff(); // Switching off the watch

		// whenever we print the object reference, we get the hexadecimal address of the
		// object
		System.out.println(w1);
		System.out.println(w2);
		System.out.println(w3);
	}

}
// please arrange the program and save the program